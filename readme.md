# LSM6DS3TR-C Rust development Docker image

Docker image for [LSM6DS3TR 6-axis (DoF) IMU accelerometer & gyroscope rust driver library](https://gitlab.com/mtczekajlo/lsm6ds3tr-rs).
